Django==1.10.2
jsonschema==2.5.1
pymongo==3.3.0
kafka==1.3.1
pytailf==1.1