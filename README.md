Virtual Proxy in charge of connecting the clients to the AQoE VLS, obtain 
network traces to analyze them and apply policies in order to regulate 
bandwidth from specific IPs

There are three different modules working together:
* Proxy: redirects the incomming HTTP traffic to the [AQoE VLS](https://gitlab.com/ontic/ericsson-aqoe-vls)
* Network Sniffing: obtain the video network traces in order to analyze them with the [Analytics Function](https://gitlab.com/ontic/ericsson-af)
* Bandwidth Regulation: applies the policies assigned by the [PGF](https://gitlab.com/ontic/ericsson-pgf-server)

The image is available in [Docker Hub](https://hub.docker.com/r/onticericssonspain/virtual-proxy/)
and can be executed in a [Docker Compose environment](https://gitlab.com/ontic/ericsson-uc3-demo2)
