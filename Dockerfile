FROM ubuntu:14.04

RUN apt-get update && apt-get install -y \
	libpcap0.8-dev \
	librrds-perl \
	rrdtool \
	wget \
	autoconf \
	libtool \
	make \
	build-essential \
	haproxy \
	python \
	python-django \
	python-pip \
	mongodb

# Get tstat
WORKDIR /tmp
RUN wget http://tstat.polito.it/download/tstat-3.1.1.tar.gz
RUN tar -xzvf tstat-3.1.1.tar.gz 
RUN mv tstat-3.1.1 /tstat
RUN rm -rf /tmp/*

# Install tstat
ADD config_files/param.h /tstat/tstat/
WORKDIR /tstat 
RUN ./autogen.sh; \
    ./configure --enable-libtstat; \
    make; \
    make install
#ADD config_files/runtime.conf tstat-conf/

# Install & configure haproxy
WORKDIR /
RUN mkdir -p /run/haproxy
RUN touch /run/haproxy/admin.sock
ADD config_files/haproxy /etc/default/
ADD config_files/haproxy.cfg /etc/haproxy/

# Install requirements
ADD config_files/requirements.txt /requirements.txt
RUN pip install -r requirements.txt
RUN rm requirements.txt

# Install mongodb & django
RUN apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv EA312927
RUN echo "deb http://repo.mongodb.org/apt/ubuntu "$(lsb_release -sc)"/mongodb-org/3.0 multiverse" | tee /etc/apt/sources.list.d/mongodb-org-3.0.list
RUN mkdir -p /data/db
ADD config_files/init_mongodb.json /data
ADD vrouter /vrouter
WORKDIR /vrouter
RUN python manage.py migrate

# Install tstat-kafka
WORKDIR /
ADD tstat_kakfa_connector /kafka_connector

EXPOSE 8080

VOLUME ["/stdin", "/data/db"]

ADD entrypoint.sh /start-container
RUN chmod +x /start-container
ENTRYPOINT /start-container
