import json
import time
from os import listdir, system

with open('config.json') as data_file:
    config = json.load(data_file)
try:
    tstat_dir = config["tstat_dir"]
    num_dir = len(listdir(tstat_dir))
    while True:
        get_directories = sorted(listdir(tstat_dir))
        new_num_dir = len(get_directories)
        current_dir = get_directories[num_dir - 1]
        if num_dir < new_num_dir:
            num_dir = new_num_dir
            system("echo \"EOF\" >> " + tstat_dir + "/" + current_dir + "/" + "log_video_complete")
        time.sleep(5)
except KeyboardInterrupt:
    print("Keyboard Interrupt")
