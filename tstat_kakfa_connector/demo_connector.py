import json
import time

from kafka import KafkaProducer


def file_reader():
    try:
        data_file = "trazas.txt"
        print("Policy applied, reading: " + data_file)
        producer = KafkaProducer(bootstrap_servers=config["kafka_ip"])
        for line in open(data_file).readlines():
            producer.send(config["kafka_topic"], bytes(line))
            print "Sent kafka:{}".format(line)
            time.sleep(15)
    except KeyboardInterrupt:
        raise KeyboardInterrupt
    except Exception as ex:
        print ex
        time.sleep(5)


with open('config.json') as config_file:
    config = json.load(config_file)

while True:
    try:
        file_reader()
    except KeyboardInterrupt:
        print("Exiting tstat_kakfa connector")
        break
    except Exception as e:
        print e
        time.sleep(5)
        pass
