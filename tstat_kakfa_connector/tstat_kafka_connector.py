import json
import time
from operator import itemgetter
from os import listdir

from kafka import KafkaProducer
from tailf import tailf


def get_last_tstat_log():
    tstat_dir = config["tstat_dir"]
    directories = sorted(listdir(tstat_dir))
    num_dir = len(listdir(tstat_dir))
    if num_dir == 0:
        return
    current_dir = directories[num_dir - 1]
    data_file = tstat_dir + "/" + current_dir + "/log_video_complete"
    return data_file


def file_reader():
    try:
        data_file = get_last_tstat_log()
        print("Reading: " + data_file)
        producer = KafkaProducer(bootstrap_servers=config["kafka_ip"])
        for line in tailf(data_file):
            if not line:
                print("File is empty!")
                break
            if line == "EOF":
                print(data_file + " ended.")
                break
            elif line[0] != "#":
                line = line.split(" ")
                # print line[0]
                if line[0] in reverse_index:
                    line[0] = reverse_index[line[0]]
                else:
                    print "{0} not at any list. Assuming {1}...".format(line[0], config["default_customer_group"])
                    line[0] = config["default_customer_group"]
                line = itemgetter(0, 16, 22, 23, 30, 51, 113)(line)
                producer.send(config["kafka_topic"], bytes(line))
                print line
    except KeyboardInterrupt:
        raise KeyboardInterrupt
    except Exception as ex:
        print ex
        time.sleep(5)


with open('config.json') as config_file:
    config = json.load(config_file)

reverse_index = {x: k for k, v in config["customer_groups"].iteritems() for x in v}

while True:
    try:
        file_reader()
    except KeyboardInterrupt:
        print("Exiting tstat_kakfa connector")
        break
    except Exception as e:
        print e
        time.sleep(5)
        pass
