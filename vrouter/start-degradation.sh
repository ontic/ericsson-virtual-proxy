#!/bin/bash
echo "starting degradation..."
tc qdisc del root dev eth1
tc qdisc add dev eth1 root handle 1: prio bands 5
tc qdisc add dev eth1 parent 1:1 handle 10:1 tbf rate 500kbit burst 10kb latency 200ms
tc qdisc add dev eth1 parent 1:2 handle 20:1 tbf rate 500kbit burst 10kb latency 200ms
tc qdisc add dev eth1 parent 1:3 handle 30:1 tbf rate 500kbit burst 10kb latency 200ms
tc qdisc add dev eth1 parent 1:4 handle 40:1 tbf rate 500kbit burst 10kb latency 200ms
tc qdisc add dev eth1 parent 1:5 handle 50:1 tbf rate 500kbit burst 10kb latency 200ms
tc filter add dev eth1 protocol ip parent 1:0 prio 1 u32 match ip dst 159.107.0.10 flowid 1:1
tc filter add dev eth1 protocol ip parent 1:0 prio 1 u32 match ip dst 159.107.0.11 flowid 1:2
tc filter add dev eth1 protocol ip parent 1:0 prio 1 u32 match ip dst 159.107.0.12 flowid 1:3
tc filter add dev eth1 protocol ip parent 1:0 prio 1 u32 match ip dst 159.107.0.13 flowid 1:4
tc filter add dev eth1 protocol ip parent 1:0 prio 1 u32 match ip dst 159.107.0.14 flowid 1:5
