#!/bin/bash
#https://linux.die.net/man/8/tc
#On this script $1 its GOLD   rate and $4 its ip or range
#               $2 its SILVER rate and $5 its ip or range
#               $3 its BRONZE rate and $6 its ip or range.

case $# in
	1)
		if [[ "$1" == @("-h"|"--help") ]];
		then
			echo ""
			echo "This script needs to be launched with 6 parmeters"
			echo "- 3 first parameters are the bandwith that will be assigned to"
			echo "  the 3 network qeues."
			echo "- the last 3 parameters are the IPs that you wanna assign to each"
			echo "  queue."
			echo ""
			echo "On this script param \$1 its GOLD   rate and param \$4 its ip or range"
			echo "               param \$2 its SILVER rate and param \$5 its ip or range"
			echo "               param \$3 its BRONZE rate and param \$6 its ip or range."
			echo ""
			echo "ie: if param \$1=4 && param \$4=192.168.20.40"
			echo "    ip 192.168.20.40 will be assigned to first network queue,"
			echo "    and this queue will have a limitation of 4mbit bandwith"
			echo ""
			echo "launch it as: "
			echo "./bw_controller.sh bw1 bw2 bw3 ip1 ip2 ip3"
			echo ""
			echo "if you wanna disable configs later, launch it as:"
			echo "./bw_controller.sh -r or --restore"
			echo ""
		else
			if [[ "$1" == @("-r"|"--restore") ]];
			then
				sudo tc qdisc del dev eth1 root
				echo "Restoring default settings with no bandwith limitations"
			else
				echo ""
				echo "bad usage of the script, try -h or --help"
				echo ""
			fi
		fi
		;;
	10)
		# borra la configuracion que haya en eth0
		sudo tc qdisc del root dev eth1
		# inicia la cola de paquetes para la interfaz de red
		sudo tc qdisc add dev eth1 root handle 1: prio bands 5
		# regula el ancho de banda
		sudo tc qdisc add dev eth1 parent 1:1 handle 10:1 tbf rate "$6"kbit buffer 1600 limit 3000
		sudo tc qdisc add dev eth1 parent 1:2 handle 20:2 tbf rate "$7"kbit buffer 1600 limit 3000
		sudo tc qdisc add dev eth1 parent 1:3 handle 30:3 tbf rate "$8"kbit buffer 1600 limit 3000
		sudo tc qdisc add dev eth1 parent 1:4 handle 40:4 tbf rate "$9"kbit buffer 1600 limit 3000
		sudo tc qdisc add dev eth1 parent 1:5 handle 50:5 tbf rate "${10}"kbit buffer 1600 limit 3000
		# se filtra por ip para encolarlo
		sudo tc filter add dev eth1 protocol ip parent 1:0 prio 1 u32 match ip dst "$1" flowid 1:1
		sudo tc filter add dev eth1 protocol ip parent 1:0 prio 1 u32 match ip dst "$2" flowid 1:2
		sudo tc filter add dev eth1 protocol ip parent 1:0 prio 1 u32 match ip dst "$3" flowid 1:3
		sudo tc filter add dev eth1 protocol ip parent 1:0 prio 1 u32 match ip dst "$4" flowid 1:4
		sudo tc filter add dev eth1 protocol ip parent 1:0 prio 1 u32 match ip dst "$5" flowid 1:5
		# sudo ./tstat-3.1.1/tstat/tstat -l -i eth0 &pyth
		echo "Setting bandwith limitations"
		echo "queue 1 $1 kbits limitation with ip $6 linked"
		echo "queue 2 $2 kbits limitation with ip $7 linked"
		echo "queue 3 $3 kbits limitation with ip $8 linked"
		echo "queue 3 $4 kbits limitation with ip $9 linked"
		echo "queue 3 $5 kbits limitation with ip ${10} linked"
		tc qdisc del root dev eth1
        tc qdisc add dev eth1 root handle 1: prio bands 5
        tc qdisc add dev eth1 parent 1:1 handle 10:1 tbf rate 10000kbit burst 10kb latency 70ms
        tc qdisc add dev eth1 parent 1:2 handle 20:1 tbf rate 2600kbit burst 10kb latency 140ms
        tc qdisc add dev eth1 parent 1:3 handle 30:1 tbf rate 2600kbit burst 10kb latency 140ms
        tc qdisc add dev eth1 parent 1:4 handle 40:1 tbf rate 1000kbit burst 10kb latency 200ms
        tc qdisc add dev eth1 parent 1:5 handle 50:1 tbf rate 1000kbit burst 10kb latency 200ms
        tc filter add dev eth1 protocol ip parent 1:0 prio 1 u32 match ip dst 159.107.0.10 flowid 1:1
        tc filter add dev eth1 protocol ip parent 1:0 prio 1 u32 match ip dst 159.107.0.11 flowid 1:2
        tc filter add dev eth1 protocol ip parent 1:0 prio 1 u32 match ip dst 159.107.0.12 flowid 1:3
        tc filter add dev eth1 protocol ip parent 1:0 prio 1 u32 match ip dst 159.107.0.13 flowid 1:4
        tc filter add dev eth1 protocol ip parent 1:0 prio 1 u32 match ip dst 159.107.0.14 flowid 1:5

		;;
	*)
		echo ""
		echo "creating degradation..."
        sudo tc qdisc del root dev eth1
		# inicia la cola de paquetes para la interfaz de red
		sudo tc qdisc add dev eth1 root handle 1: prio bands 5
		# regula el ancho de banda
		sudo tc qdisc add dev eth1 parent 1:1 handle 10:1 tbf rate "600"kbit buffer 1600 limit 3000
		sudo tc qdisc add dev eth1 parent 1:2 handle 20:2 tbf rate "600"kbit buffer 1600 limit 3000
		sudo tc qdisc add dev eth1 parent 1:3 handle 30:3 tbf rate "600"kbit buffer 1600 limit 3000
		sudo tc qdisc add dev eth1 parent 1:4 handle 40:4 tbf rate "600"kbit buffer 1600 limit 3000
		sudo tc qdisc add dev eth1 parent 1:5 handle 50:5 tbf rate "600"kbit buffer 1600 limit 3000
		# se filtra por ip para encolarlo
		sudo tc filter add dev eth1 protocol ip parent 1:0 prio 1 u32 match ip dst "159.107.0.10" flowid 1:1
		sudo tc filter add dev eth1 protocol ip parent 1:0 prio 1 u32 match ip dst "159.107.0.11" flowid 1:2
		sudo tc filter add dev eth1 protocol ip parent 1:0 prio 1 u32 match ip dst "159.107.0.12" flowid 1:3
		sudo tc filter add dev eth1 protocol ip parent 1:0 prio 1 u32 match ip dst "159.107.0.13" flowid 1:4
		sudo tc filter add dev eth1 protocol ip parent 1:0 prio 1 u32 match ip dst "159.107.0.14" flowid 1:5
		echo ""
		;;
esac
	

	# borra la configuracion que haya en eth0
	#	sudo tc qdisc del dev eth0 root
		# inicia la cola de paquetes para la interfaz de red
	#	sudo tc qdisc add dev eth0 root handle 1: prio
	#	echo "$1"
		# regula el ancho de banda
	#	sudo tc qdisc add dev eth0 parent 1:1 handle 10:1 tbf rate "$1"mbit buffer 1600 limit 3000
	#	sudo tc qdisc add dev eth0 parent 1:2 handle 20:2 tbf rate "$2"mbit buffer 1600 limit 3000
	#	sudo tc qdisc add dev eth0 parent 1:3 handle 30:3 tbf rate "$3"kbit buffer 1600 limit 3000
	#	echo "222"
		# se filtra por ip para encolarlo
	#	sudo tc filter add dev eth0 protocol ip parent 1:0 prio 1 u32 match ip src "$4" flowid 1:1
	#	sudo tc filter add dev eth0 protocol ip parent 1:0 prio 1 u32 match ip src "$5" flowid 1:2
	#	sudo tc filter add dev eth0 protocol ip parent 1:0 prio 1 u32 match ip src "$6" flowid 1:3
		# sudo ./tstat-3.1.1/tstat/tstat -l -i eth0 &pyth
