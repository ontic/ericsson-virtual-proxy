import json
import logging
import os
import time
import uuid
from subprocess import Popen, PIPE

from django.http import HttpResponse
from django.http import HttpResponseServerError
from django.http import request
from django.views.decorators.csrf import csrf_exempt
from jsonschema import Draft3Validator
from pymongo.mongo_client import MongoClient

FORMAT = '[%(asctime)s] %(levelname)s: %(message)s'
logging.basicConfig(format=FORMAT)
logger = logging.getLogger('virtual_proxy')
logger.setLevel(logging.INFO)


# python manage.py runserver

def read_schema(schema):
    schema_abs = os.path.abspath(os.path.join(os.path.dirname(os.path.abspath(__file__)), os.pardir)) + schema
    with open(schema_abs) as f:
        return json.loads(f.read())


# read info from database.
#   groups = [{group,id}]
def read_dabatase(db, location):
    logger.info('Reading database')
    out = []
    try:
        cursor = db.configuration.find_one({"_id": location})
        for i in cursor["info"]:
            out.append(i)
        print "read_database:{}".format(out)
    except Exception as e:
        logger.error('Database is offline, bypassing...')
    return out


def get_bandwidth_regulations(order_payload):
    result = []
    for item in order_payload:
        order_payload = item['plan']['policies']
        for policy in order_payload:
            if policy['policy'] == 'bandwidth_throttling':
                result.append({
                    "group": policy['group'],
                    "limit": policy['parameters']['limit']
                })
    return result


# Black magic, the super script
def regulate_bandwith(list_of_params):
    cmd_to_execute = '/vrouter/bw_controller.sh'
    for param in list_of_params:
        if type(param) == 'str':
            cmd_to_execute += ' ' + param
        else:
            cmd_to_execute += ' ' + str(param)
    cmd_to_execute += ' &'
    print(cmd_to_execute)
    # Create a subprocess
    process = Popen(cmd_to_execute, shell=True, stdin=PIPE, stderr=PIPE, stdout=PIPE)
    stdout, stderr = process.communicate()
    print(stdout, stderr)


# Check json after validation for more debugging
def validate_order(payload):
    valid_groups = ["gold", "silver corporate", "silver domestic", "bronze corporate", "bronze young"]
    valid_policies = ["bandwidth_throttling"]
    # Check the incoming json from PGF
    # Timestamp must be on time
    if "timestamp" in payload:
        timestamp = payload["timestamp"]
        now = time.time()
        after = (now + 300) * 1000
        before = (now - 300) * 1000
        if not (before <= timestamp or timestamp <= after):
            return "Value of timestamp out of time window\n" \
                   "{}<={} or {}<={} not true".format(before, timestamp, timestamp, after)

        # There must be at least one plan
        if len(payload["plans"]) < 1:
            return "Plans is empty"

        # For every plan
        for plan in payload["plans"]:
            print plan
            plan_policies = plan["plan"]["policies"]

            # At least one policy per plan
            if len(plan_policies) < 1:
                return "Policies are not included in some plan"

            # For every policy in the plan
            for policy in plan_policies:
                # Policy and group exists
                if "policy" in policy and "group" in policy:
                    # Policy and group must be in database
                    if policy["policy"] in valid_policies and policy["group"] in valid_groups:
                        # Check limit in this policy
                        if "parameters" in policy:
                            if "limit" in policy["parameters"]:
                                if not policy["parameters"]["limit"] in [64, 1000, 3000]:
                                    return "Limit value of policy not valid"
                                    # It has to be added the other policies, at this point are not validated
                    else:
                        return "An item in policies contains values not registered in db " \
                               + str(policy["group"]) + " " + str(policy["policy"])
                else:
                    return "An item in policies does not contain policy or group"

            # At least one location
            if "locations" not in plan:
                return "Location is not provided"

            # Check start and end
            if "timing_constraints" in plan:
                if "activation_time" in plan["timing_constraints"] and \
                                "deactivation_time" in plan["timing_constraints"]:
                    activation = plan["timing_constraints"]["activation_time"]
                    deactivation = plan["timing_constraints"]["deactivation_time"]
                    if activation > deactivation:
                        return "Activation time cannot be higher than deactivation time"

        return 0


@csrf_exempt
def post_order(order_payload):
    logger.info('New POST order:%s', str(order_payload))
    if order_payload.body:
        # init
        mongo_db = MongoClient().pdp
        post_validator = Draft3Validator(read_schema('/schemas/order_post.json'))

        payload = json.loads(order_payload.body)
        payload = dict(payload)

        # if schema doesnt validate
        if not post_validator.is_valid(payload):
            errors = post_validator.iter_errors(payload)
            errors_str = ""
            for error in errors:
                print str(error)
                errors_str = errors_str + " " + str(error)
            # status = 500 by default
            return HttpResponse(
                content_type="application/json",
                status=422,
                reason="Unprocessable Entity",
                content=json.dumps(
                    {"HTTP 422 Unprocessable entity": "JSON does not validate schema",
                     "schema_errors": errors_str}
                ))

        flag = validate_order(payload)
        if flag != 0:
            return HttpResponse(
                content_type="application/json",
                status=422,
                reason="Unprocessable entity",
                content=json.dumps({"HTTP 422": "Check error " + flag})
            )
        # Get group names with limits [{group,limit}]
        bandwidth_regulations = get_bandwidth_regulations(payload["plans"])
        print "bandwith_regulations\n{}".format(bandwidth_regulations)
        if not bandwidth_regulations:
            return HttpResponseServerError(
                content_type="application/json",
                content=json.dumps(
                    {"HTTP 500 Internal Server Error": "The resource cannot be created",
                     "error": "there are no bandwidth_throttling policies to aply"}
                ))

        # Get location from payload

        # Get from mongo json that correspond to location
        location_groups_info = read_dabatase(mongo_db, payload["plans"][0]["locations"][0])

        # get data from json payload
        to_mongo_info = []
        for i in location_groups_info:  # i{group,id}
            for j in bandwidth_regulations:  # j{group,limit}
                if i["group"] == j["group"]:
                    to_mongo_info.append({"ip": i["ip"], "bandwidth": j["limit"]})

        # insert order into db
        generated_uuiid = "empty"
        if len(to_mongo_info):
            generated_uuiid = str(uuid.uuid4())
            to_mongo = {"_id": generated_uuiid, "info": to_mongo_info}
            mongo_db.orders.insert_one(to_mongo)

        # apply new bandwidth restriction with regulate_bandwidth
        # With data received from order !
        params = []
        print "mongo_info={}".format(to_mongo_info)
        for i in range(2):  # i(0,1)
            for j in to_mongo_info:  # j{ip,bandwith}
                if i == 0:
                    params.append(j["ip"])
                else:
                    params.append(j["bandwidth"])

        # params should be 10 elements, 5 first ip, 5 last bw limit
        # ip=param[0] limit=param[5], and so on...
        regulate_bandwith(params)

        # return code
        response = HttpResponse(
            content_type="application/json",
            status=200,
            reason="Implementation in progress",
            content=json.dumps({"HTTP 200": flag})
        )
        response["Location"] = generated_uuiid;
        return response
    else:
        return error_path_or_request(order_payload.method, order_payload)


@csrf_exempt
def put_order(order_payload, order_id):
    logger.info('New PUT order:%s', order_payload)
    if order_payload.method == 'PUT' and order_payload.body:
        # napa
        cmd_to_execute = '/vrouter/stop-policies.sh'
        print(cmd_to_execute)
        # Create a subprocess
        process = Popen(cmd_to_execute, shell=True, stdin=PIPE, stderr=PIPE, stdout=PIPE)
        stdout, stderr = process.communicate()
        print(stdout, stderr)
        # init
        mongo_db = MongoClient().pdp
        put_validator = Draft3Validator(read_schema('/schemas/order_put.json'))

        payload = json.loads(order_payload.body)
        payload = dict(payload)

        # if schema doesnt validate
        if not put_validator.is_valid(payload):
            errors = put_validator.iter_errors(payload)
            errors_str = ""
            for error in errors:
                print str(error)
                errors_str = errors_str + " " + str(error)
            # status = 500 by default
            return HttpResponse(
                content_type="application/json",
                status=422,
                reason="Unprocessable Entity",
                content=json.dumps(
                    {"HTTP 422 Unprocessable entity": "JSON does not validate schema",
                     "schema_errors": errors_str}
                ))

        flag = validate_order(payload)
        if flag != 0:
            return HttpResponse(
                content_type="application/json",
                status=422,
                reason="Unprocessable entity",
                content=json.dumps({"HTTP 422": "Check error " + flag})
            )
        # Get group names with limits [{group,limit}]
        bandwidth_regulations = get_bandwidth_regulations(payload["plans"])
        if not bandwidth_regulations:
            return HttpResponseServerError(
                content_type="application/json",
                content=json.dumps(
                    {"HTTP 500 Internal Server Error": "The resource cannot be created",
                     "error": "there are no bandwidth_throttling policies to aply"}
                ))

        # Get location from payload

        # Get from mongo json that correspond to location
        location_groups_info = read_dabatase(mongo_db, payload["plans"][0]["locations"][0])

        # get data from json payload
        to_mongo_info = []
        for i in location_groups_info:  # i{group,ip}
            for j in bandwidth_regulations:  # j{group,limit}
                if i["group"] == j["group"]:
                    to_mongo_info.append({"ip": i["ip"], "bandwidth": j["limit"]})

        # insert order into db
        if len(to_mongo_info):
            to_mongo = {"info": to_mongo_info}
            mongo_db.orders.update_one({"_id": order_id}, {"$set": to_mongo})

        # apply new bandwidth restriction with regulate_bandwidth
        # With data received from order !
        params = []
        for i in range(2):  # i(0,1)
            for j in to_mongo_info:  # j{ip,bandwith}
                if i == 0:
                    params.append(j["ip"])
                else:
                    params.append(j["bandwidth"])

        # params should be 10 elements, 5 first ip, 5 last bw limit
        # ip=param[0] limit=param[5], and so on...
        regulate_bandwith(params)

        # return code
        response = HttpResponse(
            content_type="application/json",
            status=200,
            reason="Implementation in progress",
            content=json.dumps({"HTTP 200": flag})
        )
        return response
    else:
        return error_path_or_request(order_payload.method, order_payload)


def error_path_or_request(method="None", order_payload="Empty"):
    print "ERROR: error_path_or_request.\nmethod={}\npayload={}".format(method, order_payload)
    return HttpResponseServerError(
        status=503,
        content_type="application/json",
        content=json.dumps(
            {"HTTP 503": "Service Unavailable",
             "request": str(request)}
        ))

    # Init mongo db connection and switch to pdp database
    # mongo_db = MongoClient().pdp
    #
    # # Create json validators
    # post_validator = Draft3Validator(read_schema('/schemas/order_post.json'))
    # # put_validator = Draft3Validator(read_schema('/schemas/plans_put_schema.json'))
    #
    #
    # read_dabatase(mongo_db)
