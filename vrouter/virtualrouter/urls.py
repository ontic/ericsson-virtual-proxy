from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.post_order, name='post_order'),
    url(r'^(?P<order_id>[0-9a-zA-Z\-]{36})$', views.put_order, name='put_order'),
    url(r'^', views.error_path_or_request, name='error_path')
]
