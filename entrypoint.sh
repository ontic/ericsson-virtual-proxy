    #!/bin/bash

# Start services
#/usr/bin/mongod &
service mongodb start
service haproxy restart
python /vrouter/manage.py runserver 0.0.0.0:8000 &

# Start tstat
cd /
tstat -l -i eth1 &

sleep 10

# Import mongo
until mongoimport --db pdp --collection configuration --drop --file /data/init_mongodb.json; do
  echo "Mongo still not up, retrying in 5..."
  sleep 5
done


# Start tstat-conector
cd /kafka_connector
python file_checker.py &
python tstat_kafka_connector.py
